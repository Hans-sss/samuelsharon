from django.test import TestCase, Client
from .models import Events, Members

# Create your tests here.
class Test(TestCase):
    #Event Form
    def test_url_eventform_exist(self):
        response = Client().get('/events/add-event/')
        self.assertEquals(response.status_code, 200)

    def test_complete_eventform(self):
        response = Client().get('/events/add-event/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("What's the Event", html_kembalian)
        # self.assertIn("Add It!", html_kembalian)

    def test_template_eventform_exist(self):
        response = Client().get('/events/add-event/')
        self.assertTemplateUsed(response, 'events/add-event.html')
    

    #Member form
    def test_url_memberform_exist(self):
        response = Client().get('/events/add-member/')
        self.assertEquals(response.status_code, 200)

    def test_complete_memberform(self):
        response = Client().get('/events/add-member/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Write Your Name", html_kembalian)
        # self.assertIn("Add Me!", html_kembalian)

    def test_template_memberform_exist(self):
        response = Client().get('/events/add-member/')
        self.assertTemplateUsed(response, 'events/add-member.html')


    #Events list
    def test_url_eventslist_exist(self):
        response = Client().get('/events/')
        self.assertEquals(response.status_code, 200)

    def test_complete_eventslist(self):
        response = Client().get('/events/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Upcoming Events", html_kembalian)
        self.assertIn("Add Event", html_kembalian)

    def test_template_eventslist_exist(self):
        response = Client().get('/events/')
        self.assertTemplateUsed(response, 'events/events.html')


    #Model testing
    def test_eventmodel_exist(self):
        Events.objects.create(event='this event')
        count_events = Events.objects.all().count()
        self.assertEquals(count_events, 1)

    def test_membermodel_exist(self):
        this = Events.objects.create(event='this event')
        Members.objects.create(member='John Doe', event=this)
        count_members = Members.objects.all().count()
        self.assertEquals(count_members, 1)

    def test_eventmodel_str(self):
        event = Events.objects.create(event='this event')
        self.assertTrue(isinstance(event, Events))
        self.assertEquals(event.__str__(), event.event)

    def test_membermodel_str(self):
        event = Events.objects.create(event='this event')
        member = Members.objects.create(member='John Doe', event=event)
        self.assertTrue(isinstance(member, Members))
        self.assertEquals(member.__str__(), member.member)

    def test_eventmodel_get_absolute_url(self):
        event = Events.objects.create(event='this event')
        self.assertEquals('/events/', event.get_absolute_url())

    def test_membermodel_get_absolute_url(self):
        event = Events.objects.create(event='this event')
        member = Members.objects.create(member='John Doe', event=event)
        self.assertEquals('/events/', member.get_absolute_url())