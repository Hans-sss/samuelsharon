from django.contrib import admin
from .models import Events, Members
# Register your models here.
admin.site.register(Events)
admin.site.register(Members)
