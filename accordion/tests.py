from django.test import TestCase, Client

# Create your tests here.
class Test(TestCase):
    def test_url_exist(self):
        response = Client().get('/accordion/')
        self.assertEquals(response.status_code, 200)

    def test_text_complete(self):
        response = Client().get('/accordion/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Accordion", html_kembalian)

    def test_template_exist(self):
        response = Client().get('/accordion/')
        self.assertTemplateUsed(response, 'accordion/jquery-accordion.html')