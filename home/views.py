from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'home/index.html')

def album(request):
    return render(request, 'home/album.html')

def oldIndex(request):
    return render(request, 'home/oldIndex.html')